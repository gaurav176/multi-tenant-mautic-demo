<?php

$host   = $_SERVER['HTTP_HOST'];
$conn   =  mysqli_connect('db', 'db', 'db', 'main_db', 3306);
$sql    = "SELECT * from `tenant` where `url` = '{$host}'";
$result = mysqli_query($conn, $sql);
$Arr    = mysqli_fetch_array($result, MYSQLI_ASSOC);
$tenant = $Arr['site_name'];
$conn->close();

$local_config = '%kernel.root_dir%/config/local_'.$tenant.'.php';

$paths = [
    //customizable
    'themes'       => 'themes',
    'assets'       => 'media',
    'asset_prefix' => '',
    'plugins'      => 'plugins',
    'translations' => 'translations',
    'local_config' => $local_config,
];

//allow easy overrides of the above
if (file_exists(__DIR__.'/paths_local.php')) {
    include __DIR__.'/paths_local.php';
}

//fixed
$paths = array_merge($paths, [
    //remove /app from the root
    'root'    => substr($root, 0, -4),
    'app'     => 'app',
    'bundles' => 'app/bundles',
    'vendor'  => 'vendor',
]);
