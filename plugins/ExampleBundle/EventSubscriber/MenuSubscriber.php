<?php

declare(strict_types=1);

namespace MauticPlugin\ExampleBundle\EventSubscriber;

use Mautic\CoreBundle\CoreEvents;
use Mautic\CoreBundle\Event\MenuEvent;
use Mautic\CoreBundle\Helper\BundleHelper;
use Mautic\MarketplaceBundle\Service\Config;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class MenuSubscriber implements EventSubscriberInterface
{
    /**
     * @var BundleHelper
     */
    private $bundleHelper;

    private Config $config;

    public function __construct(Config $config, BundleHelper $bundleHelper)
    {
        $this->config       = $config;
        $this->bundleHelper = $bundleHelper;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            CoreEvents::BUILD_MENU => ['onBuildMenu', 9999],
        ];
    }

    public function onBuildMenu(MenuEvent $event): void
    {
        $items                                                              = $event->getMenuItems();
        $bundles                                                            = $this->bundleHelper->getMauticBundles(true);
        $bundles['MauticCalendarBundle']['config']['menu']['main']['items'] = [];
    }
}
