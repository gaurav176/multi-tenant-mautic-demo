<?php

return [
    'services' => [
        'fixtures' => [
            'mautic.campaign.fixture.jovy_campaign' => [
                'class'     => \MauticPlugin\ExampleBundle\DataFixtures\ORM\CampaignData::class,
                'tag'       => \Doctrine\Bundle\FixturesBundle\DependencyInjection\CompilerPass\FixturesCompilerPass::FIXTURE_TAG,
                'optional'  => true,
            ],
            'mautic.user.fixture.jovy_role' => [
                'class'     => \MauticPlugin\ExampleBundle\DataFixtures\ORM\LoadRoleData::class,
                'tag'       => \Doctrine\Bundle\FixturesBundle\DependencyInjection\CompilerPass\FixturesCompilerPass::FIXTURE_TAG,
                'arguments' => ['mautic.user.model.role'],
                'optional'  => true,
            ],
            'mautic.form.fixture.jovy_form' => [
                'class'     => \MauticPlugin\ExampleBundle\DataFixtures\ORM\LoadFormData::class,
                'tag'       => \Doctrine\Bundle\FixturesBundle\DependencyInjection\CompilerPass\FixturesCompilerPass::FIXTURE_TAG,
                'arguments' => ['mautic.form.model.form', 'mautic.form.model.field', 'mautic.form.model.action'],
            ],
        ],
    ],
];
