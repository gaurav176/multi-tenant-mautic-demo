<?php

namespace MauticPlugin\ExampleBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Persistence\ObjectManager;
use Mautic\CampaignBundle\Entity\Campaign;

class CampaignData extends AbstractFixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $campaign = new Campaign();

        $campaign->setName('Campaign B');
        $campaign->setCanvasSettings([
            'nodes' => [
              0 => [
                'id'        => '148',
                'positionX' => '760',
                'positionY' => '155',
              ],
              1 => [
                'id'        => 'lists',
                'positionX' => '860',
                'positionY' => '50',
              ],
            ],
            'connections' => [
              0 => [
                'sourceId' => 'lists',
                'targetId' => '148',
                'anchors'  => [
                  'source' => 'leadsource',
                  'target' => 'top',
                ],
              ],
            ],
          ]
        );

        $manager->persist($campaign);
        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 0;
    }

    public static function getGroups(): array
    {
        return ['group1'];
    }
}
