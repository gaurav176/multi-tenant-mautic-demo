Local setup:

- git clone git@gitlab.com:gaurav176/multi-tenant-mautic-demo.git

Run Composer with --ignore-platform-req=ext-imap to temporarily ignore these required extensions.

- composer install --ignore-platform-req=ext-imap
- ddev start

# Create table in main_db:

CREATE TABLE `tenant` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `site_name` varchar(128) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `db_name` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

References:

https://docs.mautic.org/en/setup/how-to-install-mautic/install-mautic-from-github
https://docs.mautic.org/en/setup/how-to-install-mautic/install-and-manage-mautic-with-composer
